import Vue from 'vue'
import VueRouter from 'vue-router'
import NewestView from '../views/NewestView'
import AskView from '../views/AskView'
import ShowView from '../views/ShowView'
import JobsView from '../views/JobsView'
import ItemView from '../views/ItemView.vue'
import UserView from '../views/UserView.vue'
// import createListView from '../views/createListView.js'
import bus from '../utils/bus.js'
import store from '../store'

Vue.use(VueRouter)

// const fetch = (to, from, next) => {
//   bus.$emit('start:spinner')
//   store.dispatch('FETCH_LIST', to.name)
//     .then(() => {
//       bus.$emit('end:spinner')
//       next()
//     })
//     .catch(error => {
//       console.log(error)
//     })
// }

// async function fetch(to, from, next) {
//   bus.$emit('start:spinner')
//   await store.dispatch('FETCH_LIST', to.name)
//   bus.$emit('end:spinner')
//   next()
// }

const fetch = async (to, from, next) => {
  bus.$emit('start:spinner')
  await store.dispatch('FETCH_LIST', to.name)
  bus.$emit('end:spinner')
  next()
}

const fetchDetail = async (to, from, next) => {
  bus.$emit('start:spinner')
  await store.dispatch((`FETCH_${to.name}`).toUpperCase(), to.params.id)
  bus.$emit('end:spinner')
  next()
}

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/newest',
    },
    {
      path: '/newest',
      name: 'newest',
      component: NewestView,
      beforeEnter: fetch
      // component: createListView('NewestView')
    },
    {
      path: '/ask',
      name: 'ask',
      component: AskView,
      beforeEnter: fetch
      // component: createListView('AskView')
    },
    {
      path: '/show',
      name: 'show',
      component: ShowView,
      beforeEnter: fetch
      // component: createListView('JobsView')
    },
    {
      path: '/jobs',
      name: 'jobs',
      component: JobsView,
      beforeEnter: fetch
      // component: createListView('JobsView')
    },
    {
      path: '/item/:id',
      name: 'item',
      component: ItemView,
      beforeEnter: fetchDetail
    },
    {
      path: '/user/:id',
      name: 'user',
      component: UserView,
      beforeEnter: fetchDetail
    }
  ]
})

export default router 
