import ListView from './ListView.vue'
import bus from '../utils/bus.js'
import { mapActions } from 'vuex'

export default function createListView(name) {
  return {
    // 재사용할 인스턴스(컴포넌트) 옵션들
    name,
    created() {
      bus.$emit('start:spinner')
      this.FETCH_LIST(this.$route.name)
        .then(() => {
          console.log('fetched')
          bus.$emit('end:spinner')
        })
        .catch(error => {
          console.log(error)
        })
    },
    methods: {
      ...mapActions([
        'FETCH_LIST'
      ])
    },
    render(createElement) {
      return createElement(ListView)
    }
  }
}