import axios from 'axios'

const config = {
  baseUrl: 'https://api.hnpwa.com/v0'
}

// async function request(method, url, data) {
//   try {
//     const options = {
//       method,
//       url: config.baseUrl + url,
//       data
//     }
//     return await axios(options)  
//   } catch (error) {
//     console.log(error)
//   }
// }

const request = async (method, url, data) => {
  try {
    const options = {
      method,
      url: config.baseUrl + url,
      data
    }
    return await axios(options)
  } catch (error) {
    throw error
  }
}

const fetchList = (pageName) => {
  return request('get', `/${pageName}/1.json`)
}

const fetchUserInfo = (userName) => {
  return request('get', `/user/${userName}.json`)
}

const fetchItemInfo = (id) => {
  return request('get', `/item/${id}.json`)
}

// function fetchList(pageName) {
//   return axios.get(`${config.baseUrl}/${pageName}/1.json`)
// }

// function fetchUserInfo(username) {
//   return axios.get(`${config.baseUrl}/user/${username}.json`)
// }

// async function fetchItemInfo(id) {
//   try {
//     return await axios.get(`${config.baseUrl}/item/${id}.json`)
//   } catch (error) {
//     console.log(error)
//   }
// }

export {
  fetchList,
  fetchUserInfo,
  fetchItemInfo,
}
