import { fetchList, fetchUserInfo, fetchItemInfo } from '../api'

export default {
  // promise
  // FETCH_LIST({ commit }, pageName) {
  //   return fetchList(pageName)
  //     .then(({ data }) => {
  //       console.log(4)
  //       commit('SET_LIST', data)
  //       return data
  //     })
  //     .catch(error => {
  //       console.log(error)
  //     })
  // },
  // // async1
  // async FETCH_USER({ commit }, name) {
  //   try {
  //     const response = await fetchUserInfo(name)
  //     commit('SET_USER', response.data)
  //     return response
  //   } catch (error) {
  //     console.log(error)
  //   }
  // },
  //async2
  async FETCH_LIST({ commit }, pageName) {
    const response = await fetchList(pageName)
    commit('SET_LIST', response.data)
    return response
  },
  async FETCH_USER({ commit }, name) {
    const response = await fetchUserInfo(name)
    commit('SET_USER', response.data)
    return response
  },
  async FETCH_ITEM({ commit }, id) {
    const response = await fetchItemInfo(id)
    commit('SET_ITEM', response.data)
    return response
  }
}

// export default actions